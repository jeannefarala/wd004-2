// Objects
// Data Types:
// String, Number, Boolean
// 4. Undefined - functions without return value, variables that are declared but wuithout assigned values;
// 5. null - no value
//	- explicit, if the operation cannot find a value
// 6. OBJECTS 
//	- collection of related values
//	- denoted by {}.
//	- presented in key-value pairs.

const student = {
	name: "Brandon", 
	studentNumber: "2014-15233",
	course: "BS Physics",
	college: "Science",
	department: "Physics",
	age: 22,
	isSingle: true,
	motto: "Time is gold",
	address: {
		stName: "Yakal st.",
		brgy: "North Fairview",
		city: "Quezon City",
		zipCode: 1121
	},
	showAge: function(){
		//This is an Anonymous function
		// a FUNCTION WITHIN AN OBJECT is called a METHOD
		console.log(student.age);
		return student.age;
	},
	addAge: function(){
		// Add 1 to the age property of student
		student.age += 1;
		return "Successfully added age!";
	}
	//name, studentNumber, course, college and department are keys or property keys
	//Brandon, 2014-15233, BS Physics, Science and Physics are values
};

// To access a specific key/property of an object, we will use the dot notation.

// If you use dot notation after an object that does not exist, it will cause an error.

// If we access a property that does not exist, it will return UNDEFINED.

// To add a property in an object, we will use dot notation and assign the value:

student.gender = "Male";

// To update the value of a property, we will use dot notation and assign a new value:

student.isSingle = false;

// To delete a property, we will use the keyword delete then object.propertyName:

delete student.studentNumber;

// Multidimensional Objects
// - objects within another object