// getSum
function getSum(num1, num2){
	const sum = num1 + num2;
	return sum;
}

// getDifference
function getDifference(num1, num2){
	const diff = num1 - num2;
	return diff;
}

// getProduct
function getProduct(num1, num2){
	const prod = num1 * num2;
	return prod;
}

// getQuotient
function getQuotient(num1, num2){
	const quotient = num1 / num2;
	return quotient;
}

// getCylinderVolume
function getCylinderVolume(radius, height){
	const r2 = radius * radius;
	const r2h = r2 * height;
	const pi = 3.1416;
	const cylinderVolume = pi * r2h;
	return cylinderVolume;
}

// getTrapezoidArea
function getTrapezoidArea(base1, base2, height){
	const baseSum = base1 + base2;
	const baseAve = baseSum / 2;
	const trapezoidArea = baseAve * height;
	return trapezoidArea;
}

// STRETCH GOAL

// getTotalAmount
function getTotalAmount(c,q,d,t){
	const cost = c * q;
	const discount = d / 100;
	const tax = t / 100;
	const dCost = cost * discount;
	const tCost = cost * tax;
	const disCost = cost - dCost;
	const totalCost = disCost + tCost;
	return totalCost;
}

// STRESS GOAL

// getTotalSalary
function getTotalSalary(salary, workdays, absences, lates, tax){
	const payMinute = (salary/(workdays*24*60));
	const payDaily = salary / workdays;
	const payWithAbsences = (salary - (payDaily*absences));
	const payWithLates = (payWithAbsences - (payMinute*lates));
	const netTax = 6.16/100;
	const netPay = (payWithLates - (salary*netTax));
	return netPay;
}