const bankAccount = {
	name: "Brandon Cruz",
	accountNo: "123-456",
	accountBal: 5000,
	showCustomer: function(){
		return "Customer Name: " + bankAccount.name;
	},
	showAccountNumber: function(){
		return "Customer Account Number: " + bankAccount.accountNo; 
	},
	showCustomerDetails: function(){
		return "Customer Name: " + bankAccount.name + '\n' 
		+ " Customer Account Number: " + bankAccount.accountNo + '\n'
		+ " Customer Account Balance: " + bankAccount.accountBal;
	},
	showBalance: function(){
		return "Balance: " + bankAccount.accountBal;
	},
	deposit: function deposit(amount){
		bankAccount.accountBal += amount;
		return "Deposit Successful"
	},
	withdraw: function withdraw(amount){
		bankAccount.accountBal -= amount;
		return "Withdrawal Successful"
	}
};