// Agenda:
// Review of Yesterday's Lesson
// Functions
// Objects

// Variables:
// let and const

// Data types:
// 1. String - ""
// 2. Number - we can use arithmetic
// 3. Boolean - true/false

// Arithmetic operators:
// +, -, *, /, %
// modulo - remainder

// Assignment operator: =

// A-A operators: +=, -=, *=, /=

// const num1 = 2;
// const num2 = 4;
// const sum1 = num1 + num2;
// console.log(sum1);

// const num3 = 5;
// const num4 = 3;
// const sum2 = num3 + num4;
// console.log(sum2);

// const num5 = 11;
// const num6 = 3;
// const sum3 = num5 + num6;
// console.log(sum3);


// DRY Principle
// = don't repeat yourself
//
// --> Functions


// FUNCTION
// A subprogram that is designed to do a particular task
// ex. A function that will add 2 numbers?

// Syntax:
// function nameOfTheFunction(){
// 	Task/what your function should do
// }

// *remember, function names should be DESCRIPTIVE, just like your variables. They also should be in CAMEL CASE.
	// CAMEL CASE example:
	// firstWordIsInLowerCase
// paranthesis accept PARAMETERS
// Parameters - these are the values that the function needs in order to do its task, located inside the ()
// Function declaration:

function getSum(num1, num2){
	const sum = num1 + num2;
	console.log(sum);
	return sum;
}


// function call
// Arguments - values that we will send to the functon
// getSum(2,3);
// getSum(4,5);
// getSum(9,8);

// It's good practice to declare functions before calling them.

const ageSum = getSum(15,18);
console.log(ageSum);

// Return can be for functions, variables, strings
// If the function does not have a RETURN valuem it will return an UNDEFINED data